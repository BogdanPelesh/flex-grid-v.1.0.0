# flex-grid-v.1.0.0
New future for Front-end developer flexbox grid.

#Install
You need to install <a href="https://www.npmjs.com/">npm</a> and <a href="http://gulpjs.com/">gulp</a>.

<h4>Open console and write:</h4>
<code>$ npm i</code>

<ul>
<h4>This command install plugins:</h4>
    <li>"browser-sync": "^2.17.5",</li>
    <li>"del": "^2.2.2",</li>
    <li>"gulp": "^3.9.1",</li>
    <li>"gulp-autoprefixer": "^3.1.1",</li>
    <li>"gulp-concat": "^2.6.0",</li>
    <li>"gulp-cssnano": "^2.1.2",</li>
    <li>"gulp-group-css-media-queries": "^1.2.0",</li>
    <li>"gulp-jade": "^1.1.0",</li>
    <li>"gulp-rename": "^1.2.2",</li>
    <li>"gulp-sass": "^2.3.2",</li>
    <li>"gulp-scss": "^1.4.0",</li>
    <li>"gulp-uglifyjs": "^0.6.2"</li>
 </ul>

#Usage
Open <i>src/sass/_varibles-grid.scss</i>
<ul>
<h4>You write your settings:</h4>
<li><code>$columns: 12;</code></li>
<li><code>$offset: 30px;</code></li>
<li><code>$max-width: 1280px;</code></li>
<li><code>$width-lg: 1100px;</code></li>
<li><code>$width-md: 800px;</code></li>
<li><code>$width-sm: 560px;</code></li>
<li><code>$width-xs: 360px;</code></li>
</ul>

<ul>
<h4>Use mixin with your file:</h4>
<li><code>@include flex;</code></li>
<li><code>@include column;</code></li>
<li><code>@include col-xl(x);</code> //x - nomber column</li>
<li><code>@include col-lg(x);</code></li>
<li><code>@include col-md(x);</code></li>
<li><code>@include col-sm(x);</code></li>
<li><code>@include col-xs(x);</code></li>
</ul>

<h3>scss</h3>
<code>section {
           @include flex;
           item {
               @include col-lg(4);
               @include col-md(2);
               @include col-xs(1);
           }
      }
</code>

Perfectly! You creat flex-grid ;)
