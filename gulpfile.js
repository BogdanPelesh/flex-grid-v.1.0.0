var gulp = require('gulp'),
    sass = require('gulp-sass'),
    scss = require('gulp-scss'),
    jade = require('gulp-jade'),
    browserSync = require('browser-sync'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglifyjs'),  
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    gcmq = require('gulp-group-css-media-queries'),
    del = require('del'),
    autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function(){
    return gulp.src('src/sass/**/*.+(scss|sass)')
        .pipe(sass())
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 9', 'opera 12.1'], { cascade: true }))
        .pipe(gcmq())
        .pipe(gulp.dest('src/css'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('jade', function() {
    return gulp.src('src/**/*.jade')
        .pipe(jade()) 
        .pipe(gulp.dest('src')); 
});

gulp.task('browser-sync', function() { 
    browserSync({
        server: {
            baseDir: 'src'
        },
        notify: false
    });
});

gulp.task('css-libs', ['sass'], function() {
    return gulp.src('src/css/libs.css')
        .pipe(cssnano())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('src/css'));
});

gulp.task('scripts', function() {
    return gulp.src([
        ])
        .pipe(concat('libs.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('src/js'));
});

gulp.task('watch', ['browser-sync', 'jade', 'css-libs', 'scripts'], function() {
    gulp.watch('src/sass/**/*.+(scss|sass)', ['sass']);
    gulp.watch('src/*.jade', ['jade']);
    gulp.watch('src/*.html', browserSync.reload);
    gulp.watch('src/js/**/*.js', browserSync.reload);
});

gulp.task('default', ['watch']);

gulp.task('clean', function() {
    return del.sync('dist');
});

gulp.task('clear', function () {
    return cache.clearAll();
})

gulp.task('build', ['clean', 'sass', 'scripts'], function() {

    var buildCss = gulp.src([
        'src/css/main.css',
        'src/css/libs.min.css'
        ])
    .pipe(gulp.dest('dist/css'))

    var buildFonts = gulp.src('src/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'))

    var buildJs = gulp.src('src/js/**/*')
    .pipe(gulp.dest('dist/js'))

    var buildHtml = gulp.src('src/*.html')
    .pipe(gulp.dest('dist'));

});